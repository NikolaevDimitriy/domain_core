__author__ = 'dimitriy'

import abc
from typing import Any


class Dictable:
    @abc.abstractmethod
    def to_dict(self, **kwargs) -> dict:
        pass

    @abc.abstractmethod
    def from_dict(self, **kwargs) -> Any:
        pass


# no nested objects, particular for sql
class FlatDictable:
    @abc.abstractmethod
    def to_flat_dict(self, **kwargs) -> dict:
        pass

    @abc.abstractmethod
    def from_flat_dict(self, **kwargs) -> Any:
        pass


class KeyValueItem(Dictable):

    def __init__(self, key=None, val=None):
        self.key = key
        self.value = val

    def from_dict(self, **kwargs) -> Any:
        self.key = kwargs.get('key')
        self.value = kwargs.get('value')
        return self

    def to_dict(self, **kwargs) -> dict:
        return {
            'key': self.key,
            'value': self.value
        }


class Model(FlatDictable, Dictable, metaclass=abc.ABCMeta):
    _entity_identifier = 0

    def __init__(self, **kwargs):
        self.id = kwargs.get('id')

    @property
    def is_new(self):
        return self.id is None

    @property
    def get_class(self):
        return type(self)

    @property
    def entity_identifier(self):
        return self._entity_identifier

    @property
    def safe_id(self):
        return self.id if self.id is not None else 0

    def to_flat_dict(self, **kwargs) -> dict:
        d = {
            'id': self.id,
            'entity_identifier': self.entity_identifier,
        }
        self.simple_fields_to_dict(d)
        return d

    def from_flat_dict(self, **kwargs) -> Any:
        self.id = kwargs['id']
        self.set_simple_fields(**kwargs)
        return self

    def to_dict(self, **kwargs) -> dict:
        return self.to_flat_dict(**kwargs)

    def from_dict(self, **kwargs) -> Any:
        return self.from_flat_dict(**kwargs)

    def set_simple_fields(self, **kwargs):
        pass

    def simple_fields_to_dict(self, res):
        pass


class DeletableModel(Model):

    def __init__(self, **kwargs):
        super(DeletableModel, self).__init__(**kwargs)
        self.deleted = kwargs.get('deleted', False)
        self.del_time = kwargs.get('del_time')

    def from_flat_dict(self, **kwargs) -> Any:
        super(DeletableModel, self).from_flat_dict(**kwargs)
        self.deleted = kwargs['deleted']
        self.del_time = kwargs['del_time']
        return self

    def to_flat_dict(self, **kwargs) -> dict:
        d = super(DeletableModel, self).to_flat_dict(**kwargs)
        d['deleted'] = self.deleted
        d['del_time'] = self.del_time
        return d


class Document(DeletableModel):
    creator_cls = KeyValueItem

    def __init__(self, **kwargs):
        super(DeletableModel, self).__init__(**kwargs)
        self.creation_time = kwargs.get('creation_time')
        self.upd_time = kwargs.get('upd_time')
        self.creator_id = kwargs.get('creator_id')
        self._creator = kwargs.get('creator')

    def from_dict(self, **kwargs) -> Any:
        self.from_flat_dict(**kwargs)
        self.creator = self.creator_cls().from_dict(**kwargs.get('creator', dict()))

    def to_dict(self, **kwargs) -> dict:
        d = self.to_flat_dict(**kwargs)
        d['creator'] = self.creator.to_dict()
        return d

    def from_flat_dict(self, **kwargs):
        super(Document, self).from_flat_dict(**kwargs)
        self.creation_time = kwargs.get('creation_time')
        self.upd_time = kwargs.get('upd_time')
        self.creator_id = kwargs.get('creator_id')
        return self

    def to_flat_dict(self, **kwargs):
        d = super(Document, self).to_flat_dict(**kwargs)
        d['creation_time'] = self.creation_time
        d['upd_time'] = self.del_time
        d['creator_id'] = self.creator_id
        return d

    @property
    def creator(self):
        return self._creator

    @creator.setter
    def creator(self, val):
        self._creator = val
        self.creator_id = val.id if val is not None else None
