CREATE USER domian_core_test WITH PASSWORD 'domian_core_test';

CREATE DATABASE domain_core_test
  WITH ENCODING='UTF8'
       OWNER=domian_core_test
       LC_COLLATE='ru_RU.UTF-8'
       LC_CTYPE='ru_RU.UTF-8'
       CONNECTION LIMIT=-1;
