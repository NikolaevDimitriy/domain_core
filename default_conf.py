__author__ = 'dimitriy'

DB_NAME = 'db_name'

DB_MASTER_HOST = 'db_master_host'
DB_MASTER_PORT = 'db_master_port'
DB_MASTER_USER = 'db_master_user'
DB_MASTER_PWD = 'db_master_pwd'

DB_SLAVE_HOST = 'db_slave_host'
DB_SLAVE_PORT = 'db_slave_port'
DB_SLAVE_USER = 'db_slave_user'
DB_SLAVE_PWD = 'db_slave_pwd'

REDIS_HOST = 'redis_host'
REDIS_PORT = 'redis_port'
REDIS_JSON_ENCODING = 'redis_json_encoding'

config = {
    'debug': True,
    'db': {
        DB_NAME: '',

        DB_MASTER_HOST: 'localhost',
        DB_MASTER_PORT: 5432,
        DB_MASTER_USER: 'postgres',
        DB_MASTER_PWD: 'postgres',

        DB_SLAVE_HOST: 'localhost',
        DB_SLAVE_PORT: 5432,
        DB_SLAVE_USER: 'postgres',
        DB_SLAVE_PWD: 'postgres',

        REDIS_HOST: 'localhost',
        REDIS_PORT: 6379,
        REDIS_JSON_ENCODING: 'utf8'
    }
}
