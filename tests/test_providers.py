__author__ = 'dimitriy'
import pytest
from domain_core.providers import create_data_providers, ProviderTypes


@pytest.mark.asyncio
async def test_providers_only_pg(event_loop, config):
    providers = await create_data_providers(ProviderTypes.PG, config.get('db'), event_loop)

    assert providers
    assert providers.pg_db is not None
    assert providers.redis is None


@pytest.mark.asyncio
async def test_providers_only_redis(event_loop, config):
    providers = await create_data_providers(ProviderTypes.REDIS, config.get('db'), event_loop)

    assert providers
    assert providers.pg_db is None
    assert providers.redis is not None


@pytest.mark.asyncio
async def test_providers_pg_redis(event_loop, config):
    providers = await create_data_providers(ProviderTypes.PG | ProviderTypes.REDIS, config.get('db'), event_loop)

    assert providers
    assert providers.pg_db is not None
    assert providers.redis is not None


