__author__ = 'dimitriy'

import pytest
import datetime
from domain_core.providers import create_data_providers, ProviderTypes
from domain_core.tests.storages.model_storages import UserStorage


@pytest.mark.asyncio
async def test_pg_get(event_loop, config):
    providers = await create_data_providers(ProviderTypes.PG, config.get('db'), event_loop)

    user_storage = UserStorage(providers)
    user = await user_storage.get(1)

    assert user
    assert user.id == 1
    assert user.email == 'testuser@mail.mail'
    assert user.name == 'testuser'
    assert not user.deleted
    assert user.del_time is None
    assert user.creation_date < datetime.datetime.utcnow()
