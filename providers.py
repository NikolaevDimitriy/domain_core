__author__ = 'dimitriy'

from enum import IntFlag
import default_conf as def_conf
from aioredis import create_pool as redis_pool
from asyncpg import create_pool as pg_pool


class ProviderTypes(IntFlag):
    UNDEFINED = 0,
    PG = 1,
    REDIS = 2,
    MONGO = 4,
    SEARCH = 8


class MasterSlave(object):
    def __init__(self, master, slave):
        self.master = master
        self.slave = slave


class PgProvider(MasterSlave):
    def close(self):
        self.master.close()
        self.slave.close()

    async def wait_closed(self):
        await self.master.wait_closed()
        await self.slave.wait_closed()


class DataProviders(object):

    def __init__(self, pg_db=None, redis=None, mongo=None, search=None):
        self.pg_db = pg_db
        self.redis = redis
        self.mongo = mongo
        self.search = search

    def close(self):
        self.pg_db.close()
        self.redis.close()
        self.search.close()

    async def wait_closed(self):
        await self.pg_db.wait_closed()
        await self.redis.wait_closed()
        await self.search.wait_closed()


async def create_data_providers(providers=ProviderTypes.PG | ProviderTypes.REDIS, config=None, loop=None) -> DataProviders:

    return DataProviders(pg_db=await create_pgdb_engine(loop, config) if ProviderTypes.PG in providers else None,
                         redis=await create_redis_engine(loop, config) if ProviderTypes.REDIS in providers else None,
                         mongo=None,
                         search=None)


async def create_pgdb_engine(loop, config):
    pg_master = await pg_pool(user=config.get(def_conf.DB_MASTER_USER),
                              database=config.get(def_conf.DB_NAME),
                              host=config.get(def_conf.DB_MASTER_HOST),
                              port=config.get(def_conf.DB_MASTER_PORT),
                              password=config.get(def_conf.DB_MASTER_PWD),
                              loop=loop)
    pg_slave = await pg_pool(user=config.get(def_conf.DB_SLAVE_USER),
                             database=config.get(def_conf.DB_NAME),
                             host=config.get(def_conf.DB_SLAVE_HOST),
                             port=config.get(def_conf.DB_SLAVE_PORT),
                             password=config.get(def_conf.DB_SLAVE_PWD),
                             loop=loop)
    return PgProvider(pg_master, pg_slave)


async def create_redis_engine(loop, config):
    pool = await redis_pool((config.get(def_conf.REDIS_HOST), config.get(def_conf.REDIS_PORT)), db=1,
                            encoding=config.get(def_conf.REDIS_JSON_ENCODING), loop=loop)
    return pool
#
# async def create_serche_engine(loop):
#     search_engine = await aiomysql.create_pool(host=ls.SEARCH_ENGINE_HOST,
#                                                port=ls.SEARCH_ENGINE_PORT,
#                                                loop=loop,
#                                                user='root', password='',
#                                                charset='utf8', use_unicode=True)
#     return search_engine