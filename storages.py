__author__ = 'dimitriy'
import abc
import ujson as json
from sqlalchemy.dialects import postgresql
from json_helper import serialize_entity
from domain_core.providers import DataProviders

pgdialect = postgresql.dialect()


class BaseReadingStorage(metaclass=abc.ABCMeta):
    model = None

    @abc.abstractmethod
    async def get(self, pk):
        return None

    @abc.abstractmethod
    async def get_list(self, **kwargs):
        return []


class BaseWritingStorage(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    async def save(self, instance, **kwargs):
        return False

    @abc.abstractmethod
    async def delete(self, instance):
        return False


class PgBaseReadingStorage(BaseReadingStorage):

    get_func = ''
    get_list_func = ''

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.pg_db = data_providers.pg_db

    async def get_list(self, **kwargs):
        lst = await  self.pg_fetch(self.get_list_func, *self.get_list_args(**kwargs))
        return lst

    async def get(self, pk):
        instance = await self.pg_first(self.get_func, pk)
        return instance

    def get_list_args(self, **kwargs):
        return [v for k, v in kwargs.items()]

    async def pg_first(self, query, *args):
        async with self.pg_db.slave.acquire() as conn:
            res = await conn.fetchrow(query, *args)
            if res is not None:
                return self.model().from_flat_dict(**dict(res))
            return None

    async def pg_fetch(self, query, *args):
        async with self.pg_db.slave.acquire() as conn:
            res = [self.model().from_flat_dict(**dict(row)) async for row in conn.fetch(query, *args)]
            return res


class PgBaseWritingStorage(BaseWritingStorage):

    save_func = ''
    delete_func = ''

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.pg_db = data_providers.pg_db

    async def save(self, instance, **kwargs):
        pass

    async def delete(self, instance):
        pass

    async def pg_execute(self, query, *args):
        async with self.pg_db.master.acquire() as conn:
            res = await conn.execute(query, *args)
            return res

    async def pg_scalar(self, query, *args):
        async with self.pg_db.master.acquire() as conn:
            res = await conn.fetchval(query, *args)
            return res


class RedisBaseStorageMixin:

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.redis = data_providers.redis

    async def save_to_redis(self, instance, expire=0):
        key = instance.get_redis_key()
        if key:
            try:
                data = serialize_entity(instance)
                async with self.redis.get() as r:
                    await r.set(key, data, expire=expire)
                    return True
            except:
                return False
        return False

    async def get_from_redis(self, key):
        if key:
            try:
                async with self.redis.get() as r:
                    raw = await r.get(key)
                    if raw is not None:
                        res = self.model().from_dict(**json.loads(raw))
                        return res
            except Exception as x:
                return None
        else:
            return None

    async def delete_from_redis(self, key):
        if key:
            try:
                async with self.redis.get() as r:
                    await r.delete(key)
                    return True
            except:
                return False
        else:
            return False

    async def bulk_save_to_redis(self, instances, expire=0):
        pass



# wait motor 1.0 release
class MongoBaseStorageMixin:
    def __init__(self, data_providers: DataProviders, **kwargs):
        self.mongo = data_providers.mongo

    async def save_to_mongo(self, instance):
        try:
            data = serialize_entity(instance)
            return True
        except:
            return False

    async def get_from_mongo(self, key):
        try:
            return None
        except:
            return None

    async def delete_from_mongo(self, key):
        try:
            return True
        except:
            return False


class PgRedisBaseStorage(RedisBaseStorageMixin):
    save_to_redis_if_not_exist = True

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.pg_db = data_providers.pg_db
        self.redis = data_providers.redis

    async def save(self, instance, fields=(), insert_if_not_new=False, expire=0):
        res = await super(PgRedisBaseStorage, self).save(instance, fields, insert_if_not_new)
        if res:
            res = await self.save_to_redis(instance, expire)
        return res

    async def get(self, pk):
        if pk is not None and pk != 0:
            instance = self.model(id=pk)
            res = await self.get_from_redis(instance.get_redis_key())
            if res is not None:
                return res
            res = await super(PgRedisBaseStorage, self).get(pk)
            if self.save_to_redis_if_not_exist and res is not None:
                await self.save_to_redis(res)
            return res
        return None

    async def delete(self, instance):
        res = await super(PgRedisBaseStorage, self).delete(instance)
        if res:
            res = await super(PgRedisBaseStorage, self).delete_from_redis(instance.get_redis_key())
        return res



