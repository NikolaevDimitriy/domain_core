__author__ = 'dimitriy'
from typing import Any
from domain_core.models import DeletableModel, Document


class User(DeletableModel):

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        self.name = kwargs.get('name')
        self.email = kwargs.get('email')
        self.creation_date = kwargs.get('creation_date')

    def from_flat_dict(self, **kwargs) -> Any:
        super(User, self).from_flat_dict(**kwargs)
        self.set_simple_fields(**kwargs)
        return self

    def to_flat_dict(self, **kwargs) -> dict:
        res = super(User, self).to_flat_dict(**kwargs)
        self.simple_fields_to_dict(res)
        return res

    def from_dict(self, **kwargs) -> Any:
        super(User, self).from_dict(**kwargs)
        self.set_simple_fields(**kwargs)
        return self

    def to_dict(self, **kwargs) -> dict:
        res = super(User, self).to_dict()
        self.simple_fields_to_dict(res)
        return res

    def set_simple_fields(self, **kwargs):
        self.name = kwargs.get('name')
        self.email = kwargs.get('email')
        self.creation_date = kwargs.get('creation_date')

    def simple_fields_to_dict(self, res):
        res['name'] = self.name
        res['email'] = self.email
        res['creation_date'] = self.creation_date


class Order(Document):

    def __init__(self, **kwargs):
        super(Order, self).__init__(**kwargs)
        self.price = kwargs.get('price')
        self.description = kwargs.get('description')
        self.user_id = kwargs.get('user_id')
        self.user = kwargs.get('user')

    def from_flat_dict(self, **kwargs):
        super(Order, self).from_dict(**kwargs)
        self.set_simple_fields(**kwargs)
        return self

    def to_flat_dict(self, **kwargs):
        res = super(Order, self).to_flat_dict()
        self.simple_fields_to_dict(res)
        return res

    def from_dict(self, **kwargs) -> Any:
        super(Order, self).from_dict(**kwargs)
        self.set_simple_fields(**kwargs)
        self.user = User().from_dict(**kwargs.get('user'))
        return self

    def to_dict(self, **kwargs) -> dict:
        res = super(Order, self).to_dict()
        self.simple_fields_to_dict(res)
        res['user'] = self.user.to_dict()
        return res

    def set_simple_fields(self, **kwargs):
        self.price = kwargs.get('price')
        self.description = kwargs.get('description')
        self.user_id = kwargs.get('user_id')

    def simple_fields_to_dict(self, res):
        res['price'] = self.price
        res['description'] = self.description
        res['user_id'] = self.user_id
