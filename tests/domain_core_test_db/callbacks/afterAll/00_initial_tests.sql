/* pgmigrate-encoding: utf-8 */
DO $$ begin raise notice 'initial tests'; end; $$;


CREATE OR REPLACE FUNCTION test_user_order()
RETURNS void AS
$BODY$
DECLARE creator_id integer;
        order_id integer;
        usr RECORD;
        ordr RECORD;

BEGIN
  select user_save(
    0::integer,
    'email@mail.mail'::text,
    'name'::text,
    now() at time zone 'utc'
  ) into creator_id;

  assert creator_id > 0;

  select order_save(
    0::integer,
    1500::decimal(8,2),
    creator_id::integer,
    'descript'::text,
    now() at time zone 'utc'
  ) into order_id;

  assert order_id > 0;
  perform user_del(creator_id);
  select * into usr from user_get(creator_id);

  assert usr.id = creator_id;
  assert usr.name = 'name';
  assert usr.deleted = TRUE;
  assert usr.del_time is not null;
  perform order_del(order_id);
  select * into ordr from order_get(order_id);
  assert ordr.id = order_id;
  assert ordr.description = 'descript';
  assert ordr.deleted = TRUE;
  assert ordr.del_time is not null;

  delete from orders where id = order_id;
  delete from users where id = creator_id;
END;
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION exec_tests()
    RETURNS void
    LANGUAGE 'plpgsql'
    VOLATILE
AS $function$
begin
perform test_user_order();
end
$function$;

CREATE OR REPLACE FUNCTION create_test_env()
    RETURNS void
    LANGUAGE 'plpgsql'
    VOLATILE
AS $function$
DECLARE creator_id integer;

begin
  select user_save(
    0::integer,
    'testuser@mail.mail'::text,
    'testuser'::text,
    now() at time zone 'utc'
  ) into creator_id;

  assert creator_id > 0;

  perform order_save(
    0::integer,
    1500::decimal(8,2),
    creator_id::integer,
    'description'::text,
    now() at time zone 'utc'
  );
  perform order_save(
    0::integer,
    1600::decimal(8,2),
    creator_id::integer,
    'description1'::text,
    now() at time zone 'utc'
  );
end
$function$;

