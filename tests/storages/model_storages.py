__author__ = 'dimitriy'

from storages import PgBaseReadingStorage, PgBaseWritingStorage
from domain_core.tests.models.models import User, Order


class UserStorage(PgBaseReadingStorage):
    model = User
    get_func = 'select * from user_get($1::integer)'


class OrderStorage(PgBaseWritingStorage):
    model = Order
