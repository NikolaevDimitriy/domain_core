/* pgmigrate-encoding: utf-8 */
DO $$ begin raise notice 'initial migration'; end; $$;

CREATE TABLE users (
  id serial PRIMARY KEY,
  email text UNIQUE NOT NULL,
  name text NOT NULL,
  deleted boolean NOT NULL DEFAULT FALSE,
  creation_date timestamp without time zone NOT NULL,
  del_time timestamp without time zone
)
WITH(
  OIDS = FALSE
);

CREATE TABLE orders (
  id serial PRIMARY KEY,
  price decimal(8,2) NOT NULL,
  creator_id integer REFERENCES users (id) NOT NULL,
  description text,
  deleted boolean NOT NULL DEFAULT FALSE,
  creation_date timestamp without time zone,
  del_time timestamp without time zone
)
WITH(
  OIDS = FALSE
);

CREATE OR REPLACE FUNCTION user_save(
identity integer,
email text,
name text,
creation_date timestamp without time zone)
RETURNS integer AS
$BODY$
DECLARE res_id integer;
BEGIN
  if identity > 0 then
      update users set email = email, name = name where id = identity;
      res_id := identity;   
  else
    insert into users (email, name, creation_date)
    values (email, name, creation_date) returning id into res_id;
  end if;
  RETURN res_id;
END;
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION user_get(identity integer)
RETURNS TABLE (
  id integer,
  email text,
  name text,
  deleted boolean,
  creation_date timestamp without time zone,
  del_time timestamp without time zone
) AS
$BODY$
BEGIN
  RETURN QUERY
  select * from users where users.id = identity;
END;
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION user_del(identity integer)
RETURNS void AS
$BODY$
BEGIN
  update users set del_time = now() at time zone 'utc', deleted = true where id = identity;
END;
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION users_get(paginator_limit integer, paginator_offset integer)
RETURNS TABLE (
  id integer,
  email text,
  name text,
  deleted boolean,
  creation_date timestamp without time zone,
  del_time timestamp without time zone
) AS
$BODY$
BEGIN
  RETURN QUERY
  select * from users order by id limit paginator_limit offset paginator_offset;
END;
$BODY$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION order_save(
  identity integer,
  price decimal(8,2),
  creator_id integer,
  description text,
  creation_date timestamp without time zone
)
RETURNS integer AS
$BODY$
DECLARE res_id integer;
BEGIN
  if identity > 0 then
      update orders set price = price, description = description where id = identity;
      res_id := identity;
  else
    insert into orders(price, creator_id, description, creation_date)
    values (price, creator_id, description, creation_date) returning id into res_id;
  end if;
  RETURN res_id;
END;
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION order_get(identity integer)
RETURNS TABLE (
  id integer,
  price decimal(8,2),
  creator_id integer,
  description text,
  deleted boolean,
  creation_date timestamp without time zone,
  del_time timestamp without time zone
) AS
$BODY$
BEGIN
  RETURN QUERY
  select * from orders where orders.id = identity;
END;
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION order_del(identity integer)
RETURNS void AS
$BODY$
BEGIN
  update orders set del_time = now() at time zone 'utc', deleted = true where id = identity;
END;
$BODY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION orders_get(paginator_limit integer, paginator_offset integer)
RETURNS TABLE (
  id integer,
  price decimal(8,2),
  creator_id integer,
  description text,
  deleted boolean,
  creation_date timestamp without time zone,
  del_time timestamp without time zone
) AS
$BODY$
BEGIN
  RETURN QUERY
  select * from orders order by id limit paginator_limit offset paginator_offset;
 END;
$BODY$ LANGUAGE plpgsql;

INSERT INTO ops (op) VALUES ('migration V0001__initial.sql');
