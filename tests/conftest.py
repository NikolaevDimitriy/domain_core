__author__ = 'dimitriy'
import asyncio
import pytest
import yaml
from domain_core.tests.models.models import User, Order
from domain_core.default_conf import config as default_conf


@pytest.fixture
def user():
    return User()


@pytest.fixture
def order():
    return Order()


@pytest.fixture
def config():
    try:
        with open('./tests/testconfig.yml', 'r') as ymlfile:
            conf = yaml.load(ymlfile)
            extend_dict(conf, default_conf)
            return conf

    except Exception as x:
        return default_conf


def extend_dict(extend_me, extend_by):
    if isinstance(extend_by, dict):
        for k, v in extend_by.items():
            if k in extend_me:
                extend_dict(extend_me.get(k), v)
            else:
                extend_me[k] = v
    else:
        extend_me += extend_by
